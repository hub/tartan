/*

How to use?
***********

If you have Nix installed, you can get an environment with everything
needed to compile tartan by running:

    $ nix-shell

at the root of the tartan repository.

You can also compile tartan and ‘install’ it by running:

    $ nix-build

at the root of the tartan repository. The command will install
tartan to a location under Nix store and create a ‘result’ symlink
in the current directory pointing to the in-store location.

The dependencies are pinned, you can update them to latest versions with:

    $ nix-shell --run 'niv update'

How to tweak default arguments?
*******************************

Nix supports the ‘--arg’ option (see nix-build(1)) that allows you
to override the top-level arguments.

For instance, to use your local copy of Nixpkgs:

    $ nix-build --arg pkgs "import $HOME/Projects/nixpkgs {}"

Or to speed up the build by not running the test suite:

    $ nix-build --arg doCheck false

*/

{
  # Nixpkgs instance, will default to one from Niv.
  pkgs ? null,
  # Whether to run tests when building Tartan using nix-build.
  doCheck ? true,
  # Whether to build Tartan, or shell for developing it.
  # We do not use lib.inNixShell because that would also apply
  # when in a nix-shell of some package depending on this one.
  shell ? false,
} @ args:

let
  # Pinned Nix dependencies (e.g. Nixpkgs) managed by Niv.
  sources = import ./nix/sources.nix;

  # Setting pkgs to the pinned version
  # when not overridden in args.
  pkgs =
    if args.pkgs or null == null
    then
      import sources.nixpkgs {
        overlays = [];
        config = {};
      }
    else args.pkgs;

  inherit (pkgs) lib;

  # Function for building Tartan or shell for developing it.
  makeDerivation =
    if shell
    then pkgs.mkShell
    else pkgs.stdenv.mkDerivation;
in
makeDerivation rec {
  name = "tartan";

  src =
    let
      # Avoid copying the following to the Nix store:
      # - build/ directory, since Nixpkgs’ Meson infrastructure would want to use it
      # - .git/, _build/ and subprojects/ directories, since they would unnecessarily bloat the source
      cleanSource = path: _: !lib.elem (builtins.baseNameOf path) ["_build" "build" ".git" "subprojects"];
    in
    if shell
    then null
    else builtins.filterSource cleanSource ./.;

  # Dependencies running on the build platform.
  nativeBuildInputs = with pkgs; [
    meson
    ninja
    pkg-config
    python3
  ] ++ lib.optionals shell [
    niv
  ];

  # Dependencies running on the host platform.
  buildInputs = with pkgs; [
    gobject-introspection
    glib
    llvmPackages_16.libclang
    llvmPackages_16.libllvm
  ];

  inherit doCheck;
}
